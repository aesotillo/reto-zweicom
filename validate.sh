#!/bin/bash

# Script validación estado de servicio
# AS - Dic 2020

status=`curl -s http://localhost:5000 | grep -o  "hello" | wc -l`

valida() {
c_name=$1
if [ ${status} = 1 ]; then
        echo "Servicio disponible"
        echo
else
        echo "Servicio no se encuentra disponible. Reiniciando contenedor..."
        docker restart ${c_name}
        echo
        echo "Contenedor reiniciado"
fi
}

valida zweicom-test
