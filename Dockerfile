FROM python:3.8-slim-buster

WORKDIR /app

COPY ["requirements.txt", "service.py", "/app/"]
RUN pip install --trusted-host pypi.python.org -r requirements.txt

EXPOSE 5000

CMD [ "python", "./service.py" ]
