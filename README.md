## Test Técnico DevOps Zweicom - Alejandro Sotillo

Proyecto: https://gitlab.com/aesotillo/reto-zweicom

Imágenes docker: https://hub.docker.com/repository/docker/aesotillo/zweicom-test/tags

URL pública del servicio: http://161.35.5.104:5000/

### 1. Diseñe e implemente, utilizando la herramienta que prefiera (bash, ansible, chef, terraform, etc), una forma de disponibilizar infraestructura, que puede ser máquina virtual, contenedor docker o cloud.

En primera instancia se creó una imagen Docker y fue disponibilizada en Docker Hub. Se incluye archivo Dockerfile.

Se creó plataforma en cloud DigitalOcean utilizando IaC Terraform.

Los archivos para la creación de la plataforma están en el directorio **tf**

El archivo **provider.tf** contiene la información sobre el proveedor que usará Terraform.

**Nota:** La variable do_token fue exportada localmente con el valor del Token de Acceso Personal de DigitalOcean para poder gestionar los recursos cloud.

El archivo **01_ssh_key.tf** contiene la creación de una llave en DigitalOcean que será utilizada para conectarse al Droplet (Instancia).

El archivo **02_droplet.tf** contiene todo el detalle necesario para la creación del Droplet, a mencionar, la imagen a utilizar (en este caso ubuntu 18.04), el nombre del droplet, la región, el tamaño, la llave que utilizará para la conexión mediante ssh y por último un archivo .yaml que contiene la configuración de inicio del Droplet.

**Nota:** Para correr los archivos .tf y crear los distintos recursos será necesario ejecutar **terraform plan** y en caso de haber conformidad, ejecutar **terraform apply**.


### 2. Sobre la infraestructura se debe instalar de forma automática el servicio entregado. Por ejemplo: si la infraestructura es de tipo contenedores, la solución debe crear los contenedores, instalar dependencias, instalar servicio, etc.

En el archivo **userdata.yaml** mencionado arriba se instalan las dependencias necesarias, se crean algunos archivos y se ejecuta el contenedor a partir de la imagen previamente creada.

Además, este archivo userdata.yaml también ejecuta los pasos necesarios para habilitar la validación continua del servicio.


### 3. Se deben agregar validaciones automáticas del servicio post instalación.

Se creó un script en bash **validate.sh** que a través de curl valida el estado del servicio.

Este script se ejecuta cada minuto mediante **cron**

Si en la ejecución del script la salida contiene el string "hello", da por sentado que el servicio está disponible, en caso contrario, ejecuta el reinicio del contenedor.

**Nota:** Es posible probar esto haciendo un **docker stop zweicom-test**, entonces la ejecución automática del script detectará que el servicio no está disponible y gatillará el reinicio del contenedor.



