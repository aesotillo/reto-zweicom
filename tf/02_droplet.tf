resource "digitalocean_droplet" "docker_test" {
  image     = "ubuntu-18-04-x64"
  name      = "docker-test"
  region    = "nyc1"
  size      = "s-1vcpu-1gb"
  user_data = "${file("userdata.yaml")}"
  ssh_keys  = ["${digitalocean_ssh_key.asotillo.fingerprint}"]
}
